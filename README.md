# Boa Files Implementation

## Instruções

Upload de arquivos CSV para o bigQuery e integração com Data Studio

#### Construa o ambiente

    docker-compose up --b --d

#### Rode as migrations

      docker-compose exec web flask db init
      docker-compose exec web flask db migrate
      docker-compose exec web flask db upgrade

#### REST API

Se tudo deu certo, a um health-check estara disponivel no endereço http://localhost:5000/health-check



