import os

from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask import (
    Flask,
    jsonify,
    send_from_directory,
    request,
    redirect,
    url_for
)

from flask_cors import CORS
from flask_jwt_extended import JWTManager
from .model import User, configure as config_db
from .serializer import configure as config_ma
from .endpoints.bp_user import bp_user

def create_app():
    app = Flask(__name__)
    app.config.from_object("project.config.Config")
    CORS(app)
    config_db(app)
    config_ma(app)
    JWTManager(app)
    Migrate(app, app.db)

    app.register_blueprint(bp_user)
    
    @app.shell_context_processor
    def inject_models():
        return {
            'User': User,
        }

    return app
