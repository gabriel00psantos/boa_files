from marshmallow import fields, post_load, validate
from flask_marshmallow import Marshmallow
from project.model import User

ma = Marshmallow()


def configure(app):
    ma.init_app(app)


class UserListSchema(ma.Schema):
    class Meta:
        model = User
    user_id = fields.Str(dump_only=True)
    username = fields.Str(required=False)
    email = fields.Str(required=False)

    @post_load
    def make_middleman(self, data, **kwargs):
        return User(**data)

class LoginSchema(UserListSchema):
    email = fields.Str(required=True)
    password = fields.Str(required=True)
    
class UserInsertUpdateSchema(LoginSchema):
    user_id = fields.Str(required=False)
    username = fields.Str(required=True)
    

