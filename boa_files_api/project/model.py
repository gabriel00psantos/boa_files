from flask_sqlalchemy import SQLAlchemy
from passlib.hash import pbkdf2_sha256
from sqlalchemy.dialects.postgresql import UUID
import uuid
import datetime
import enum

db = SQLAlchemy()


def configure(app):
    db.init_app(app)
    app.db = db

class User(db.Model):
    __tablename__ = "user"
    #Set to String to avoid problems with sqlite db on tests
    user_id = db.Column(db.String(36), 
                                     default=lambda: str(uuid.uuid4()), 
                                     primary_key=True)
    username = db.Column(db.String(255), nullable=True)
    password = db.Column(db.String(255), nullable=False)
    email = db.Column(db.String(255), unique=True, nullable=False)
    created_at = db.Column(db.DateTime, default=datetime.datetime.utcnow)

    def gen_hash(self):
        self.password = pbkdf2_sha256.hash(self.password)

    def verify_password(self, password):
        return pbkdf2_sha256.verify(password, self.password)

