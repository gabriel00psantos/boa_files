import time
from flask.cli import FlaskGroup
from project import create_app

app = create_app()

cli = FlaskGroup(app)

@cli.command("create_db")
def create_db():
	try:
		app.db.drop_all()
		app.db.create_all()
		app.db.session.commit()
		print('DB created :D')
	except Exception as e:
		print('Create DB Error :(')
		print(e)

if __name__ == "__main__":
    cli()